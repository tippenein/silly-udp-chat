#!/usr/bin/env python

import cgi
import cgitb
import time
import simplejson as json
from socket import *
cgitb.enable()

form = cgi.FieldStorage()
udp = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)
#domain='kh2170-09.cselabs.umn.edu'
#port = 8888
BUFS = 1024
client = {}

def main():
  if form.has_key('action'):
    action = form['action'].value
    log(action)
    if ( action == 'login' ):
      login(form)
    elif (action == 'logout' ):
      logout(form)
      print_msg( "logged out" )
    elif (action == 'send' ):
      send(form)
      print_msg( "success" )
    elif (action == 'update'):
      update(form)
    else:  
      print_msg( "something else" )

  # write html for the first time
  else:
    html = open('chat.html')
    print_msg( html.read() )
    html.close()

#Login
def login(form):
  try:
    user      = form['user'].value
    password  = form['password'].value
    domain    = form['domain'].value
    port      = form['port'].value
  except:
    print_msg('failed') 
  d = _dumps(req='login',domain=domain,port=port,usr=user,passwd=password) 
  usend(udp, d, domain, port)
  sid = udp.recv(BUFS)
  log(sid)
  if sid.startswith('!valid'):
    print_msg('failed')
  else:  
    print_msg(sid)

#Update
def update(form):
  try:
    domain    = form['domain'].value
    port      = form['port'].value
    user      = form['user'].value
    d = _dumps(req='update', usr=user, domain=domain, port=port) 
    log(d)
    usend(udp, d, domain, port)
    res = udp.recv(BUFS)
    print_msg(res)
  except:
    udp.close()  
    print_msg( 'logged out' )
  
#Send  
def send(form):
  domain  = form['domain'].value
  port    = form['port'].value
  sid     = form['session'].value
  user    = form['user'].value
  msg     = form['msg'].value
  log("user: ".format(user))
  d = _dumps(req='msg', usr=user,domain=domain, port=port, sid=sid, msg=msg) 
  usend(udp, d, domain, port)
  print_msg( msg )
  
#Logout
def logout(form):
  domain    = form['domain'].value
  port      = form['port'].value
  sid       = form['session'].value
  user      = form['user'].value
  log("sid: {}".format(sid) )
  d = _dumps(req='logout',domain=domain,port=port,usr=user,sid=sid) 
  log( d )
  usend(udp, d, domain, port)
  udp.close()
  print_msg( 'logged out' )

### CONVENIENCE FUNCTIONS ###
def usend(sock,msg,domain,port):
  port = int(port)
  sock.sendto(msg, (domain, port))

def print_msg(msg):
  print '''Content-type: text/html\n\n{}'''.format(msg)

def log(txt):
  with open('log.txt', 'a') as f:
    f.write("{}\n".format(txt))

def _dumps(req=None, msg=None, sid=None, domain=None, port=None, usr=None, passwd=None, timestamp=int(time.time())):
  ''' If no timestamp is specified, the time.time() value is sent 
      update is the only method that receives a altered timestamp from here 
  '''
  d = {'req':req,'sid':sid,'domain':domain,'port':port,'msg':msg,'usr':usr,'passwd':passwd, 'timestamp':timestamp }
  return json.dumps(d)

if __name__ == '__main__':
  main()
