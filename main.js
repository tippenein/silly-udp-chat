var query_results 
var $ = function(id) { return document.getElementById(id) }

function postEscape(val) {
  return encodeURIComponent(val).replace(/%20/g, "+")
}

//////////////Send Msg//////////////////
function submit_send() {
  var user = $('user').value
  var msg = $('message').value  
  var domain = $('domain').value 
  var port = $('port').value
  request = new XMLHttpRequest()
  request.onreadystatechange = respond_send
  request.open("POST", "chat.cgi", true )
  request.setRequestHeader("Content-type","application/x-www-form-urlencoded")
  request.send( "action=send&msg=" + postEscape(msg) + "&user=" + postEscape(user) + 
          "&session=" + postEscape(session) + "&domain=" + postEscape(domain) + 
          "&port=" + postEscape(port) )
}
function respond_send() {
  if ( request.readyState != 4 ) return
  $('status').innerHTML = "message sent" 
  $('message').value = '' 
}
/////////////////LOGIN////////////////
function submit_login() {
  var user = $('user').value 
  var password = $('password').value
  var domain = $('domain').value 
  var port = $('port').value
  request = new XMLHttpRequest()
  request.onreadystatechange = respond_login
  request.open("POST", "chat.cgi", true )
  request.setRequestHeader("Content-type","application/x-www-form-urlencoded")
  request.send( "action=login&password=" + postEscape( password ) +
          "&user=" + postEscape( user ) + "&domain=" + postEscape( domain ) +
          "&port=" + postEscape( port ) )
}
function respond_login() {
  if( request.readyState != 4 ) return
  session = request.responseText.trim()
  if( session != "failed" ) {
    $('status').innerHTML = "Welcome - domain: "+$('domain').value + 
            ":"+$('port').value
    $('login').hidden = true
    $('logout').hidden = false
    $('inputs').hidden = true
    submit_update()
  } else 
    $('status').innerHTML = "Incorrect Username or Password"
}
/////////////UPDATE//////////////
function submit_update() {
  try {
  var user = $('user').value 
  var domain = $('domain').value 
  var port = $('port').value
  request = new XMLHttpRequest()
  request.onreadystatechange = respond_update
  request.open("POST", "chat.cgi", true )
  request.setRequestHeader("Content-type","application/x-www-form-urlencoded")
  request.send( "action=update&user=" + postEscape(user) +  
        "&domain=" + postEscape(domain) + "&port=" + postEscape(port) )
  } catch (error) { submit_logout() }
}

function respond_update() {
  if( request.readyState != 4 ) return
  res = request.responseText
  $('messages').value = res 
  setTimeout(function() { submit_update() }, 6000) 
}
////////////////LOGOUT///////////////
function submit_logout() {
  var domain  = $('domain').value
  var port    = $('port').value
  var user    = $('user').value
  request = new XMLHttpRequest()
  request.onreadystatechange = respond_logout
  request.open("POST", "chat.cgi", true )
  request.setRequestHeader("Content-type","application/x-www-form-urlencoded")
  request.send( "action=logout&session=" + postEscape(session) + 
          "&user=" + postEscape(user) + "&domain=" + postEscape(domain) + 
          "&port=" + postEscape(port) )
}

function respond_logout() {
  if( request.readyState != 4 ) return
  //session = request.responseText.trim()
  $('login').hidden = false
  $('logout').hidden = true
  $('inputs').hidden = false
  clearAllFields()
  $('status').innerHTML = "Successfully Logged out"
  session = null
}

function clearAllFields() {
  $('user').value = ""
  $('password').value = ""
  $('domain').value = ""
  $('port').value = ""
  $('messages').value = ""
}
 
window.onload=function() {
  clearAllFields()
  $('user').focus()
  $('login').hidden = false  
  $('logout').hidden = true 
}
