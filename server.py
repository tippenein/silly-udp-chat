import socket
import select
import simplejson as json
from random import randint
import time

host = '0.0.0.0'
port = 8888
users = 'users.txt'

socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
socket.bind((host, port))
BUFS = 512
clients = {}
timeoutInterval = 300
messages = []

def main():
  print "listening on {}:{}".format(host,port)
  quit = False
  while not quit:
    data, addr = socket.recvfrom(BUFS)
    j = json.loads(data)
    req = j['req']
    if data:
      #LOGIN
      if req=='login':
        login(j, addr)
      #MSG   
      elif req=='msg':
        msg(j, addr)
      #UPDATE
      elif req=='update':
        update(j, addr)  
      #LOGOUT
      elif req=='logout':
        logout(j, addr)
      else: 
        print "received message: {}".format(data)  
    else:
      pass    
  socket.close()

def logout(data, addr):
  sid = data['sid']
  messages.append(json.dumps(data))
  print data
  try: del clients[sid]
  except: pass

def login(data, addr):
  usr     = data['usr']
  passwd  = data['passwd']
  print "usr: {}\npass: {}\n".format(usr, passwd)
  if passwd == get_pass(usr):
    sid = generate_sessionID()
    messages.append(json.dumps(data))
    socket.sendto(sid, addr) 
    clients[sid] = getTime()
  else: 
    socket.sendto('!valid', addr) 
    print "invalid credentials"

def msg(data, addr):
  sid = data['sid']
  if verify(sid):
    messages.append(json.dumps(data))
    clients[sid] = getTime() 
  else:
    print "{} timed out".format(sid)
  
def update(data, addr):    
  timestamp = int(data['timestamp'])
  msg=''
  print_active_clients()
  for message in messages:
    #print "messages ts: {}".format(get_timestamp(message))
    #print "user last ts: {}".format( timestamp )
    if get_timestamp(message) < timestamp:
      print "putting msg on queue: {}".format (msg)
      msg += format_msg(message)
    else:
      print "old message: {}".format(msg) 
  socket.sendto(msg, addr) 

def get_timestamp(message):
  ''' grab the utc from the front of a message '''
  d = json.loads(message)
  ts = d['timestamp']
  return int(ts)

def format_msg(message):
  ''' setup message for printing on client side '''
  d   = json.loads(message)
  usr = d['usr']
  msg = d['msg']
  req = d['req']
  if req == 'login':
    return '<< ' + usr + ' >> - ' + 'logged in!' + '\n'
  if req == 'logout':
    return '[ ' + usr + ' ] - ' + 'logged out :(' + '\n'
  else:
    return '< ' + usr + ' > ' + msg + '\n'

def generate_sessionID():  
  e = str(randint(1,9))                                     
  for i in xrange(0,10): 
    e += str(randint(0,9)) 
  return e 

def print_active_clients():
  c=''
  for key in clients.keys():
    c+="sid: " + str(key) + ", timestamp: " + str(clients[key]) + "\n"
  print "active clients: {}".format(c)  
        
def verify(sid):
  timeoutInterval = 300
  # compare timestamp to now
  if (clients[sid] < (getTime()-timeoutInterval)):
    del clients[sid]
    return False
  else: return True    

def getTime():
  return int(time.time() )
   
def get_pass(usr):
  with open(users, 'r') as f:
    for line in f.readlines():
      if line.split(' ')[0] == usr:
        return line.split(' ')[1].strip('\n')
       
if __name__ == '__main__':
  import sys
  main()
